# USCSS-Nostromo



USCSS Nostromo was constructed in 2101 as an interstellar cruiser, but was refitted in 2116 as a commercial towing vehicle.Nostromo was subsequently operated as a commercial hauler, transporting<br/>automated ore and oil refineries between the outer colonies and Earth. As part of this retrofit, the ship's original Saturn J-3000 engines were removed and replaced with the two immensely powerful <br/>

Rolls-Royce N66 Cyclone thrust engines, with bipolar vectoring for midline lift function. Each of these N66 engines developed 65,830 metric tons of thrust, using water for reaction mass. When running at full power, the engines produced a high impulse thrust total of 131,660 kN.<br/>

Seven hypersleep chambers that would accommodate the crew during long-duration flight were also installed at this time. Many of the ship's background systems, including its autopilot feature, were controlled by MU/TH/UR 6000, an artificial intelligence computer mainframe.

**Final voyage**

The USCSS Nostromo departed from Earth in 2120 and made a one month trip to Neptune where it connected with the cargo hauler. From there, the Nostromo ultimately departed on its scheduled eight month interstellar journey to Thedus where it spent eight weeks while the hauler was loaded before its extended trip back to the Solar System, due to added weight.<br/>

During the tug's return trip, the Weyland-Yutani Corporation detected and partially decoded an unidentified warning signal emanating from Acheron (LV-426),and the Nostromo was selected (unbeknownst to its crew) to investigate and hopefully recover a potential Xenomorph specimen from the moon, placing the crew ten months away from Earth. <br/> To help ensure the recovery of the creature, Nostromo's science officer was replaced two days before the ship left Thedus with Ash, a Synthetic sleeper agent who, under Special Order 937, was to assist in the recovery of a Xenomorph by any means necessary, even at the expense of the rest of the crew. Upon leaving Thedus, Captain Arthur Dallas transmitted the ship's report packet back to Sol, relaying it through Sevastopol Station.

**The Nostromo above LV-426**
Nostromo set down on LV-426 on June 3, 2122. The ship was damaged during landing when dust entered one of the engine intakes, causing it to overheat and eventually triggering an electrical fire in the engineering section. <br/> 

When Executive Officer Thomas Kane was later brought back to the ship with a Facehugger attached to him, Ash broke quarantine procedure by allowing Dallas and Navigation Officer Joan Lambert to carry him aboard. Dallas elected to take off from LV-426, despite the fact several non-critical systems had yet to be repaired.
