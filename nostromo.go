package main

import (
    "fmt"
    "net/http"
    "os"
)

const (
    apiURL = "https://api.nostromo.com"
    username = os.Getenv("GITLAB_USERNAME")
    password = os.Getenv("GITLAB_PASSWORD")
)

func main() {
    client := http.Client{}

    req, err := http.NewRequest("GET", apiURL+"/data", nil)
    if err != nil {
        panic(err)
    }

    req.SetBasicAuth(username, password)

    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()

    fmt.Println(resp.Status)
}
